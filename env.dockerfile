FROM continuumio/miniconda3

RUN conda config --set always_yes True

COPY conda-packages.txt ./
RUN conda create --name api-cooperative $(cat conda-packages.txt)

ENV PATH /opt/conda/envs/api-cooperative/bin:$PATH

COPY node-packages.txt .
RUN npm install -g $(cat node-packages.txt)

COPY pip-packages.txt ./
RUN pip install $(cat pip-packages.txt)
RUN pip install --no-cache-dir 'git+https://github.com/kennknowles/python-jsonpath-rw.git@b008a97c247cd0f309e85db98e37d0c14e8dc81c#egg=jsonpath_rw'

WORKDIR /src/dtop-shared-library
COPY src/dtop-shared-library .
RUN python setup.py sdist --dist-dir=/dist
RUN pip install dtop-shared-library --find-links=/dist
