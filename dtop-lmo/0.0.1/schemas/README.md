What is missing?
	- Downtime due to failure:
		- should it be a JSON schema by itself? Or should it be included in Operation.yaml?
	- Most of the port and vessel properties:
		- Wait until everything is defined to implement.
	- Equipment JSON schema.
