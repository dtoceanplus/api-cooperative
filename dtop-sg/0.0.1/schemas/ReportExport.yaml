ReportExport:
  type: object
  required:
    - section_request
    - table_of_contents
    - study_params
  properties:
    checklist_summary:
      type: array
      items:
        type: string
    section_request:
      type: object
      properties:
        applicant_answers:
          type: boolean
        applicant_summary:
          type: boolean
        assessor_overall_scores:
          type: boolean
        assessor_question_categories:
          type: boolean
        assessor_scores_comments:
          type: boolean
        checklist_outstanding_activities:
          type: boolean
        checklist_stage_results:
          type: boolean
        checklist_summary:
          type: boolean
        improvement_areas:
          type: boolean
        metric_results:
          type: boolean
    study_params:
      type: object
      properties:
        date:
          type: string
        description:
          type: string
        selected_stage:
          type: string
        selected_stage_gate:
          type: string
        stage_index:
          nullable: true
          allOf:
            - type: integer
              example: 0
        study_name:
          type: string
        threshold_settings:
          type: string
    table_of_contents:
      type: object
      properties:
        alignment:
          type: string
        ul:
          type: array
          items:
            anyOf:
              - type: string
              - type: object
                properties:
                  type:
                    type: string
                  ul:
                    type: array
                    items:
                      type: string
    applicant_answers:
      type: array
      items:
        anyOf:
          - type: string
          - $ref: '#/Text'
          - $ref: '#/Svg'
          - $ref: '#/Ul'
          - $ref: '#/TableExample'
          - $ref: '#/Columns'
          - $ref: '#/Image'
    applicant_summary:
      type: array
      items:
        anyOf:
          - type: string
          - $ref: '#/Text'
          - $ref: '#/Svg'
          - $ref: '#/Ul'
          - $ref: '#/TableExample'
          - $ref: '#/Columns'
          - $ref: '#/Image'
    assessor_scores_comments:
      type: array
      items:
        anyOf:
          - type: string
          - $ref: '#/Text'
          - $ref: '#/Svg'
          - $ref: '#/Ul'
          - $ref: '#/TableExample'
          - $ref: '#/Columns'
          - $ref: '#/Image'
    improvement_areas:
      type: array
      items:
        anyOf:
          - type: string
          - $ref: '#/Text'
          - $ref: '#/Svg'
          - $ref: '#/Ul'
          - $ref: '#/TableExample'
          - $ref: '#/Columns'
          - $ref: '#/Image'
    metric_results:
      type: array
      items:
        anyOf:
          - type: string
          - $ref: '#/Text'
          - $ref: '#/Svg'
          - $ref: '#/Ul'
          - $ref: '#/TableExample'
          - $ref: '#/Columns'
          - $ref: '#/Image'
    outstanding_activities:
      type: array
      items:
        anyOf:
          - type: string
          - $ref: '#/Text'
          - $ref: '#/Svg'
          - $ref: '#/Ul'
          - $ref: '#/TableExample'
          - $ref: '#/Columns'
          - $ref: '#/Image'
    assessor_overall_scores:
      $ref: '#/PlotlyChart'
    assessor_question_categories:
      $ref: '#/PlotlyChart'
    checklist_bar_charts:
      type: object
      properties:
        activity_category:
          $ref: '#/PlotlyChart'
        evaluation_area:
          $ref: '#/PlotlyChart'

Text:
  type: object
  properties:
    text:
      type: string
    fontSize:
      type: number
    alignment:
      type: string
    pageBreak:
      type: string
    style:
      type: string

Svg:
  type: object
  properties:
    svg:
      type: string
    width:
      type: number
    alignment:
      type: string

Ul:
  type: array
  items:
    type: string

TableExample:
  type: object
  properties:
    style:
      type: string
    table:
      type: object
      properties:
        headerRows:
          type: integer
        widths:
          type: array
          items:
            type: string
        body:
          type: array
          items:
            anyOf:
              - type: array
                items:
                  type: object
                  properties:
                    text:
                      type: string
                    style:
                      type: string
                    fillColor:
                      type: string
              - type: array
                items:
                  anyOf:
                    - type: object
                      properties:
                        text:
                          type: string
                        italics:
                          type: string
                    - type: string


Columns:
  type: object
  properties:
    columns:
      type: array
      items:
        anyOf:
          - type: object
            properties:
              svg:
                type: string
              width:
                type: number
              alignment:
                type: string
          - type: object
            properties:
              text:
                type: string
              width:
                type: number
          - type: object
            properties:
              text:
                type: array
                items:
                  anyOf:
                    - type: object
                      properties:
                        text:
                          type: string
                        color:
                          type: string
                        style:
                          type: string
                    - type: string
                    - type: object
                      properties:
                        text:
                          type: string
                        link:
                          type: string
                        color:
                          type: object
                          properties:
                            decoration:
                              type: string
                            lineHeight:
                              type: number
              alignment:
                type: string
              width:
                type: string
              lineHeight:
                type: number
    margin:
      type: array
      items:
        type: number

Image:
  type: object
  properties:
    image:
      type: string
    width:
      type: number
    alignment:
      type: string

PlotlyChart:
  type: object
  properties:
    data:
      type: array
      items:
        anyOf:
          - type: object
            properties:
              name:
                type: string
              orientation:
                type: string
              type:
                type: string
              x:
                type: array
                items:
                  type: number
              y:
                type: array
                items:
                  type: string
    layout:
      type: object
      properties:
        layout:
          type: object
          properties:
            autosize:
              type: boolean
            font:
              type: object
              properties:
                color:
                  type: string
            height:
              type: number
            margin:
              type: object
              properties:
                pad:
                  type: number
            template:
              type: string
            title:
              type: object
              properties:
                text:
                  type: string
                x:
                  type: number
                xanchor:
                  type: string
            width:
              type: number
            xaxis:
              type: object
              properties:
                range:
                  type: array
                  items:
                    type: number
                tickmode:
                  type: string
                ticktext:
                  type: array
                  items:
                    type: string
                tickvals:
                  type: array
                  items:
                    type: number
                title:
                  type: string
            yaxis:
              type: object
              properties:
                automargin:
                  type: boolean

object:
  $ref: '#/ReportExport'
