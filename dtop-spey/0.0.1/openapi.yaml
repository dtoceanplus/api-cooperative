openapi: 3.0.2

info:
  title: DTOceanPlus System Performance and Energy Yield (SPEY) API
  description: |
    This service provides functionality to create
    System Performance and Energy Yield (SPEY) assessments.
  contact:
    name: Tecnalia R&I - Vincenzo Nava
    email: vincenzo.nava@tecnalia.com
  license:
    name: Apache 2.0
    url: https://www.apache.org/licenses/LICENSE-2.0.html
  version: 0.0.1

servers:
  - url: 'http://localhost:{port}'
    description: 'Local server'
    variables:
      port:
        default: '5000'

tags:
  - name: alternative-metrics
  - name: assessment
  - name: efficiency
  - name: energy-production
  - name: power-quality
  - name: representation


paths:
  /spey:
    $ref: 'paths/spey.yaml'
  /spey/{speyId}:
    $ref: 'paths/spey-assessment.yaml'
  /spey/{speyId}/inputs:
    $ref: 'paths/spey-inputs.yaml'
  /spey/{speyId}/efficiency:
    $ref: 'paths/efficiency.yaml'
  /spey/{speyId}/energy-production:
    $ref: 'paths/energy-production.yaml'
  /spey/{speyId}/alternative-metrics:
    $ref: 'paths/alternative-metrics.yaml'
  /spey/{speyId}/power-quality:
    $ref: 'paths/power-quality.yaml'
  # /representation:
  #   $ref: 'paths/representation.yaml'
  /representation/{speyId}:
    $ref: 'paths/representation-project.yaml'
  /representation/filters:
    $ref: 'paths/representation-filters.yaml'

components:
  schemas:


    SPEY-Assessment:
      $ref: schemas/SPEY-Assessment.yaml#/object

    Representation-Project:
      $ref: schemas/Representation-Project.yaml#/object

    URL:
      $ref: schemas/URL.yaml

    Efficiency:
      $ref: schemas/Efficiency.yaml#/object
    Energy-Production:
      $ref: schemas/Energy-Production.yaml#/object
    Alternative-Metrics:
      $ref: schemas/Alternative-Metrics.yaml#/object
    Power-Quality:
      $ref: schemas/Power-Quality.yaml#/object
    Error:
      $ref: schemas/Error.yaml

    Aggregation-Level:
      $ref: schemas/Enums.yaml#/Aggregation-Level

    wave_flux:
      $ref: schemas/SPEY-Inputs.yaml#/wave_flux

    current_flux:
      $ref: schemas/SPEY-Inputs.yaml#/current_flux

    wave_EJPD:
      $ref: schemas/SPEY-Inputs.yaml#/wave_EJPD

    current_scenarii:
      $ref: schemas/SPEY-Inputs.yaml#/current_scenarii

    sc_farm:
      $ref: schemas/SPEY-Inputs.yaml#/sc_farm

    ec_farm:
      $ref: schemas/SPEY-Inputs.yaml#/ec_farm

    ec_devices:
      $ref: schemas/SPEY-Inputs.yaml#/ec_devices

    ed_results:
      $ref: schemas/SPEY-Inputs.yaml#/ed_results

    downtime_hours:
      $ref: schemas/SPEY-Inputs.yaml#/downtime_hours

    et_results:
      $ref: schemas/SPEY-Inputs.yaml#/et_results

    device_characteristics_general:
      $ref: schemas/SPEY-Inputs.yaml#/device_characteristics_general

    device_characteristics_machine:
      $ref: schemas/SPEY-Inputs.yaml#/device_characteristics_machine

    device_characteristics_dimensions:
      $ref: schemas/SPEY-Inputs.yaml#/device_characteristics_dimensions

    rated_power_flux:
      $ref: schemas/Attributes.yaml#/rated_power_flux
    array_capt_eff:
      $ref: schemas/Attributes.yaml#/array_capt_eff
    device_capt_eff:
      $ref: schemas/Attributes.yaml#/device_capt_eff


    array_abs_transf_eff:
      $ref: schemas/Attributes.yaml#/array_abs_transf_eff
    device_abs_transf_eff:
      $ref: schemas/Attributes.yaml#/device_abs_transf_eff
    array_rel_transf_eff:
      $ref: schemas/Attributes.yaml#/array_rel_transf_eff
    device_rel_transf_eff:
      $ref: schemas/Attributes.yaml#/device_rel_transf_eff


    array_abs_deliv_eff:
      $ref: schemas/Attributes.yaml#/array_abs_deliv_eff
    array_rel_deliv_eff:
      $ref: schemas/Attributes.yaml#/array_rel_deliv_eff


    array_captured_wetted:
      $ref: schemas/Attributes.yaml#/array_captured_wetted
    device_captured_wetted:
      $ref: schemas/Attributes.yaml#/device_captured_wetted
    array_transformed_wetted:
      $ref: schemas/Attributes.yaml#/array_transformed_wetted
    device_transformed_wetted:
      $ref: schemas/Attributes.yaml#/device_transformed_wetted
    array_delivered_wetted:
      $ref: schemas/Attributes.yaml#/array_delivered_wetted
    array_captured_mass:
      $ref: schemas/Attributes.yaml#/array_captured_mass
    device_captured_mass:
      $ref: schemas/Attributes.yaml#/device_captured_mass
    array_transformed_mass:
      $ref: schemas/Attributes.yaml#/array_transformed_mass
    device_transformed_mass:
      $ref: schemas/Attributes.yaml#/device_transformed_mass
    array_delivered_mass:
      $ref: schemas/Attributes.yaml#/array_delivered_mass
    PWR:
      $ref: schemas/Attributes.yaml#/PWR
    array_CL:
      $ref: schemas/Attributes.yaml#/array_CL
    device_CL:
      $ref: schemas/Attributes.yaml#/device_CL
    array_CL_rated_power:
      $ref: schemas/Attributes.yaml#/array_CL_rated_power
    device_CL_rated_power:
      $ref: schemas/Attributes.yaml#/device_CL_rated_power
    array_CL_ratio:
      $ref: schemas/Attributes.yaml#/array_CL_ratio
    device_CL_ratio:
      $ref: schemas/Attributes.yaml#/device_CL_ratio
    IA_ratio:
      $ref: schemas/Attributes.yaml#/IA_ratio
    Export_ratio:
      $ref: schemas/Attributes.yaml#/Export_ratio
    Cable_ratio:
      $ref: schemas/Attributes.yaml#/Cable_ratio

    array_lifetime_gross_energy_pd:
      $ref: schemas/Attributes.yaml#/array_lifetime_gross_energy_pd
    array_annual_gross_energy_pd:
      $ref: schemas/Attributes.yaml#/array_annual_gross_energy_pd
    array_monthly_gross_energy_pd:
      $ref: schemas/Attributes.yaml#/array_monthly_gross_energy_pd
    device_lifetime_gross_energy_pd:
      $ref: schemas/Attributes.yaml#/device_lifetime_gross_energy_pd
    device_annual_gross_energy_pd:
      $ref: schemas/Attributes.yaml#/device_annual_gross_energy_pd
    device_monthly_gross_energy_pd:
      $ref: schemas/Attributes.yaml#/device_monthly_gross_energy_pd

    array_lifetime_lost_energy_pd:
      $ref: schemas/Attributes.yaml#/array_lifetime_lost_energy_pd
    array_annual_lost_energy_pd:
      $ref: schemas/Attributes.yaml#/array_annual_lost_energy_pd
    array_monthly_lost_energy_pd:
      $ref: schemas/Attributes.yaml#/array_monthly_lost_energy_pd
    device_lifetime_lost_energy_pd:
      $ref: schemas/Attributes.yaml#/device_lifetime_lost_energy_pd
    device_annual_lost_energy_pd:
      $ref: schemas/Attributes.yaml#/device_annual_lost_energy_pd
    device_monthly_lost_energy_pd:
      $ref: schemas/Attributes.yaml#/device_monthly_lost_energy_pd


    array_lifetime_net_energy_pd:
      $ref: schemas/Attributes.yaml#/array_lifetime_net_energy_pd
    array_annual_net_energy_pd:
      $ref: schemas/Attributes.yaml#/array_annual_net_energy_pd
    array_monthly_net_energy_pd:
      $ref: schemas/Attributes.yaml#/array_monthly_net_energy_pd
    device_lifetime_net_energy_pd:
      $ref: schemas/Attributes.yaml#/device_lifetime_net_energy_pd
    device_annual_net_energy_pd:
      $ref: schemas/Attributes.yaml#/device_annual_net_energy_pd
    device_monthly_net_energy_pd:
      $ref: schemas/Attributes.yaml#/device_monthly_net_energy_pd
    array_lifetime_net_ratio_pd:
      $ref: schemas/Attributes.yaml#/array_lifetime_net_ratio_pd
    array_lifetime_lost_ratio_pd:
      $ref: schemas/Attributes.yaml#/array_lifetime_lost_ratio_pd
    array_annual_net_ratio_pd:
      $ref: schemas/Attributes.yaml#/array_annual_net_ratio_pd
    array_annual_lost_ratio_pd:
      $ref: schemas/Attributes.yaml#/array_annual_lost_ratio_pd
    array_monthly_net_ratio_pd:
      $ref: schemas/Attributes.yaml#/array_monthly_net_ratio_pd
    array_monthly_lost_ratio_pd:
      $ref: schemas/Attributes.yaml#/array_monthly_lost_ratio_pd

    device_lifetime_net_ratio_pd:
      $ref: schemas/Attributes.yaml#/device_lifetime_net_ratio_pd
    device_lifetime_lost_ratio_pd:
      $ref: schemas/Attributes.yaml#/device_lifetime_lost_ratio_pd
    device_annual_net_ratio_pd:
      $ref: schemas/Attributes.yaml#/device_annual_net_ratio_pd
    device_annual_lost_ratio_pd:
      $ref: schemas/Attributes.yaml#/device_annual_lost_ratio_pd
    device_monthly_net_ratio_pd:
      $ref: schemas/Attributes.yaml#/device_monthly_net_ratio_pd
    device_monthly_lost_ratio_pd:
      $ref: schemas/Attributes.yaml#/device_monthly_lost_ratio_pd

    device_transformed_phase:
      $ref: schemas/Attributes.yaml#/device_transformed_phase
    array_transformed_phase:
     $ref: schemas/Attributes.yaml#/array_transformed_phase
    array_delivered_phase:
      $ref: schemas/Attributes.yaml#/array_delivered_phase

    array_captured_lease:
      $ref: schemas/Attributes.yaml#/array_captured_lease
    array_transformed_lease:
      $ref: schemas/Attributes.yaml#/array_transformed_lease
    array_delivered_lease:
      $ref: schemas/Attributes.yaml#/array_delivered_lease


  responses:
    Standard500ErrorResponse:
      $ref: responses/Standard500ErrorResponse.yaml
    ResourceDoesNotExistResponse:
      $ref: responses/ResourceDoesNotExistResponse.yaml
    200:
      $ref: responses/200.yaml
    201:
      $ref: responses/201.yaml
    400:
      $ref: responses/400.yaml
    404:
      $ref: responses/404.yaml

  parameters:
    speyId:
      $ref: parameters/speyId.yaml

  requestBodies:
    Assessment:
      $ref: requestBodies/assessment.yaml
