openapi: '3.0.2'
info:
  description: |
    This is the System Lifetime Costs module API
    that goes with the server code
  version: 0.1.0
  title: SLC API
  contact:
    name: WavEC - Luis Amaral
    email: luis.amaral@wavec.org
  license:
    name: Apache 2.0
    url: https://www.apache.org/licenses/LICENSE-2.0.html

servers:
  - url: 'http://localhost:{port}'
    description: 'Local server'
    variables:
      port:
        default: '5000'

tags:
  - name: benchmark
  - name: bom
  - name: economical
  - name: financial
  - name: inputs
  - name: project
  - name: representation
  - name: results

paths:
  # Manage projects
  /api/projects:
    $ref: "paths/project.yaml"
  /api/projects/{slcid}:
    $ref: "paths/project_id.yaml"

  # Upload inputs
  /api/projects/{slcid}/inputs/general:
    $ref: 'paths/inputs_general.yaml'
  /api/projects/{slcid}/inputs/financial:
    $ref: "paths/inputs_financial.yaml"
  /api/projects/{slcid}/inputs/ace:
    $ref: "paths/inputs_ace.yaml"
  /api/projects/{slcid}/inputs/external:
    $ref: "paths/inputs_external.yaml"

  # Results
  /api/projects/{slcid}/results:
    $ref: 'paths/results.yaml'

  # Bill of Materials
  /api/projects/{slcid}/billofmaterials:
    $ref: 'paths/bom.yaml'

  # ECONOMICAL
  /api/projects/{slcid}/economical/capex:
    $ref: 'paths/capex.yaml'
  /api/projects/{slcid}/economical/opex:
    $ref: 'paths/opex.yaml'
  /api/projects/{slcid}/economical/lcoe:
    $ref: 'paths/lcoe.yaml'
  /api/projects/{slcid}/economical/ace:
    $ref: 'paths/ace.yaml'

  # FINANCIAL
  /api/projects/{slcid}/financial/pbp:
    $ref: 'paths/pbp.yaml'
  /api/projects/{slcid}/financial/irr:
    $ref: 'paths/irr.yaml'
  /api/projects/{slcid}/financial/npv:
    $ref: 'paths/npv.yaml'

  # BENCHMARK
  /api/projects/{slcid}/benchmark/metrics:
    $ref: 'paths/benchmark/metrics_per_kw.yaml'
  /api/projects/{slcid}/benchmark/breakdown:
    $ref: 'paths/benchmark/lcoe_breakdown.yaml'

  # DIGITAL REPRESENTATION
  # /representation:
  #   $ref: 'paths/representation.yaml'
  /representation/{id}:
    $ref: 'paths/representation_id.yaml'


components:
  schemas:
    # Output schemas
    Results:
      $ref: 'schemas/Results.yaml#/object'

    CapEx:
      $ref: 'schemas/capex.yaml#/object'
    OpEx:
      $ref: 'schemas/opex.yaml#/object'
    LCOE:
      $ref: 'schemas/lcoe.yaml#/object'
    ace:
      $ref: 'schemas/ace.yaml#/object'
    Payback_Period:
      $ref: 'schemas/payback_period.yaml#/object'
    Internal_Rate_Return:
      $ref: 'schemas/internal_rate_return.yaml#/object'
    Net_Present_Value:
      $ref: 'schemas/net_present_value.yaml#/object'
    Metrics_kw:
      $ref: 'schemas/benchmark.yaml#/Metrics_kw'
    LCOE_breakdown:
      $ref: 'schemas/benchmark.yaml#/LCOE_breakdown'

    # Input schemas
    SLCProject:
      $ref: 'schemas/SLCProject.yaml#/object'

    ET_Array:
      oneOf:
        - $ref: '../../dtop-et/0.0.1/openapi.yml#/paths/~1energy_transf~1{etId}~1array/get/responses/200/content/application~1json/schema'
        - $ref: 'schemas/URL.yaml'
    ED_Outputs:
      oneOf:
        - $ref: '../../dtop-ed/0.0.1/openapi.yml#/paths/~1api~1energy-deliv-studies~1{edId}~1results/get/responses/200/content/application~1json/schema'
        - $ref: 'schemas/URL.yaml'
    SK_BOM:
      oneOf:
        - $ref: '../../dtop-sk/0.0.1/openapi.yml#/paths/~1sk~1{ProjectId}~1bom/get/responses/200/content/application~1json/schema'
        - $ref: 'schemas/URL.yaml'
    LMO_BOM:
      oneOf:
        - $ref: 'schemas/URL.yaml'
    Main_Solu:
      oneOf:
        - $ref: '../../dtop-lmo/0.0.1/openapi.yml#/paths/~1api~1{lmoid}~1phases~1{phase}~1plan/get/responses/200/content/application~1json/schema'
        - $ref: 'schemas/URL.yaml'
    Energy-Production:
      oneOf:
        - $ref: '../../dtop-spey/0.0.1/openapi.yml#/paths/~1spey~1{speyId}~1energy-production/get/responses/200/content/application~1json/schema'
        - $ref: 'schemas/URL.yaml'

    Inputs_general:
      $ref: 'schemas/Inputs_general.yaml#/object'
    Inputs_financial:
      $ref: 'schemas/Inputs_financial.yaml#/object'
    Inputs_ace:
      $ref: 'schemas/Inputs_ace.yaml#/object'
    Inputs_external:
      $ref: 'schemas/Inputs_external.yaml#/object'

    URL:
      $ref: schemas/URL.yaml

    Representation:
      $ref: 'schemas/Representation.yaml#/object'

  parameters:
    slcid:
      $ref: 'parameters/slcid.yaml'

  responses:
    Standard500ErrorResponse:
      $ref: responses/Standard500ErrorResponse.yaml
    ResourceDoesNotExistResponse:
      $ref: responses/ResourceDoesNotExistResponse.yaml
    200:
      $ref: responses/200.yaml
    201:
      $ref: responses/201.yaml
    204:
      $ref: responses/201.yaml
    400:
      $ref: responses/400.yaml
    404:
      $ref: responses/404.yaml
