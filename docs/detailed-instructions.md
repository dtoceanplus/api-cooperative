# Create your own api-cooperative repository
1. Fork the repository in a namespace you are developing your module
1. Provide a "*Reporter*" access for @dtoceanplus.integration for your fork to allow **OCC** effectively support you
   * Use *"Setting/Members.."* GitLab project web page
1. Create a branch in the cloned repository, for example `xxx-integration` (`xxx` is the short name of your module)
1. In such a branch, add the folder with your *OpenAPI* development files. The folder will be named `dtop-xxx` where `xxx` is the module (short name) you are developing.
1. Stage, commit and push the changes.
1. Ask for a merge request to OCC repository -branch `master`
1. Allow [pull](https://docs.gitlab.com/ee/workflow/repository_mirroring.html#pulling-from-a-remote-repository-starter) mirroring the `master` branch of OCC with the **protected** `master` branch of your own repository
1. Rebase your own branch in your own repository with the `master` branch of your own repository

# Advance your *OpenAPI*
1. Do all the modifications you consider appropriate **ONLY IN YOUR BRANCH OF YOUR OWN REPOSITORY**
1. Run the tests
   * `docker-compose run --rm xtest make test`
   * `docker-compose run --rm xtest pre-commit run --all-files`
1. If the tests are successful, then stage, commit and push the changes.
1. Ask for a merge request to OCC repository -branch `master`. It is important that step #2 is successful.
   * *OCC will accept only merge request that don’t break the pipeline*.
1. Wait for the OCC to integrate the changes and for the automated mirroring of OCC -`master` branch in your own `master` branch to take place
1. Rebase your own branch in your own repository with the `master` branch of your own repository

**Notes**: COMMIT and PUSH ONLY CHANGES THAT SATISFY THE TESTS
