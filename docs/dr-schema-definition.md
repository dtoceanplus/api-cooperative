# Context
The theoretical backbone of the implementation of the Digital Representation is available [here](https://opencascade.gitlab.io/dtocean/dtop_main/representation.html
), while a tutorial is available [here](representation.md).

It is extremely important that the **Digital Representation is consistent with [Deliverable D7.1](https://www.dtoceanplus.eu/content/download/4075/file/DTOceanPlus_D7.1_Standard_Data_Formats_of_OES_Tecnalia_20191029_v1.0.pdf)**, as well as that **all the modules must support the same standardized API**.

In this note, we are aiming at defining a consistent definition of the schemas of the Digital Representation, in order to achieve a standardised, consistent and complete representation of the ocean energy project.

# Procedure
1. Implement the API as described [here](representation.md).
   * The routes to be implemented are `GET /representation/{id}` and `POST /representation` . Optionally also a set of filters in the route `GET /representation/filters`.
1. Define the schemas and don't forget to copy **representation** component into **components.schemas** section. For *POST* and *GET* they should be consistent.
1. Define in the backend of the module the blueprints described in the openapi file, in order to handle, manage, restructure and export the information, provoided by your module, required to complete the Digital Representation of the whole ocean energy project.

# Consistent definition of the representation
The Digital Representation is made of objects, which are the boxes in Figures 3.5, 3.6 and 3.7 of D7.1. Each object wil lhave a set of properties as described in the Appendix of D7.1. We have also to consider that in the Ocean Energy Systems there will be several "objects" of the same type (instantiation // transversal dimension of the Digital Representation, following the definition in D7.1).

In order to account for all the above, during the TWG hold on July 2nd (see presentation [here] () we have agreed to use a standardise definition of the schemas.

## The representation schema
Each module will define a schema for representation. The representation schema in Swagger will be an *OBJECT** made of **LIST OF OBJECTS**.  The first level properties are the boxes of the Digital Representation, all written in lower case, in singular form, and when there are made of two or moore words, they are connected by "_". So for example, in the Technology Design Family we'll have the following objects:
- array
- device
   * prime_mover
      + functional_structure
      + structural_components
   * control_system
      + active
      + reactive
      + latching
      + user_defined
   * pto
      + mechanical_conversion
      + electrical_conversion
      + grid_conditioning
- power_transmission
   * transmission_to_shore
      + onshore_landing_point
      + connection
      + static_cable_segment
      + dynamic_cable_segment
         + bend_restrictor
         + bend_stiffener
         + buoyancy_module
   * array_network
      + connection
      + static_cable_segment
      + dynamic_cable_segment
         + bend_restrictor
         + bend_stiffener
         + buoyancy_module
   * collection_point
      + structure
      + foundation
      + transformer
      + switchgear
      + power_quality_equipment
- station_keeping
   * mooring_line
      + mooring_line_segment
      + connection
      + ancillary
   * foundation
      + anchor
      + fixed_structure

Each of these properties, will be an object in Swagger with five (second level) properties:
- id
- location
- properties (be aware that this is the only one with final "s")
- assessment
- hierarchical
- connection

Each second level property will have third level properties. For these third level properties will be given freedom to the moduel developers to include them according to the outputs of their module. Each third level property will be an object with three fields
- description
- value
- unit
- origin (it is the module that exports the quantity --> SC, MC, EC, ET, ED, SK, LMO, SPEY, RAMS, SLC, ESA)

Of course, a module should not produce outputs for all the properties (first, second and third level) of the Digital Representation

## Assigning id
The id will be a composed string of concatenating numbers following the vertical dimension of the Digital Representtion
Example: Mechanical Conversion 1 of the PTO 2 of the Device 3 of the Array 1, will have the following id: "1_3_2_1" from the highest to the most bottom

## Example Schema
Let us consider the output of SPEY and what refers to the Digital Represetation  for the object "power_transmission".
The piece of schema will be (not implemented yet):
``` yaml
Representation_Project:
   type: object
   properties:
      array:
         ...
      device:
         ...
      power_transmission:
         type: array
         items:
            type: object
            properties:
               id:
                  type: string
               assessment:
                  type: object
                  properties:
                     delivered_absolute_efficiency:
                        type: object
                        properties:
                           description:
                              type: string
                           value:
                              type: number
                           unit:
                              type: string
                           origin:
                              type: string
                     cable_ratio:
                        type: object
                        properties:
                           description:
                              type: string
                           value:
                              type: number
                           unit:
                              type: string
                  .
                  .
                  .
```
It nis noteworthy that SPEY, of course, will not cover all the fields of the Digital Representation. The final Digital Representation will be the reult of the export of all the modules, after merging the different contributions.
For @dtoceeanplus.integration : it is expected that the standardised "keys" will be presented in more than one module. We can merge the properties when the have the same primary key (array, device, pto) and the same id value.

## Example JSON
The output of SPEY will be like:
```json
{
    "power_transmission": [
        {
            "id": "1_1",
            "assessment": {
                "delivered_absolute_efficiency": {
                    "description": "The ratio between delivered energy and resource available",
                    "value": 0.85,
                    "unit": "-"
                },
                "cable_ratio": {
                    "description": "The ratio between the total length of the cables and delivered energy",
                    "value": 0.85,
                    "unit": "km/kWh"
                }
            }
        }
    ]
}
```
