# DTOcean+ OpenAPI cookbook
- Familiarize yourself with corresponding [Wiki](https://gitlab.com/opencascade/dtocean/dtop_example/wikis/openapi/Home) collected resources
- ["How to add description on top of an existing definition?"](#%22how-to-add-description-on-top-of-an-existing-definition%22)
- ["Is it possible to use Pandas objects and how?"](#%22is-it-possible-to-use-pandas-objects-and-how%22)

## "How to add description on top of an existing definition?"
Let's compare the following OpenAPI snippets:
```yaml
number-attribute:
  type: object
  properties:
    value:
      type: number
      format: float
    Aggregation-Level:
      $ref: Enums.yaml#/Aggregation-Level

rated-power-flux:
  allOf:
    - $ref: '#/number-attribute'
    - description: Ratio between the Rated Power of the device and the average Power production of the Device

array-capt-eff:
  allOf:
    - $ref: '#/number-attribute'
    - description: Array Capture efficiency, defined as the ratio between Annual Array Captured Energy and Rated Power of the Array

device-capt-eff:
  allOf:
    - $ref: '#/array-attribute'
    - description: Device Capture efficiency, defined as the ratio between Annual Device Captured Energy and Rated Power of the Device

Efficiency:
  type: object
  properties:
    rated-power-flux:
      $ref: #/rated-power-flux
    array-capt-eff:
      $ref: #/array-capt-eff
    device-capt-eff:
      $ref: #/device-capt-eff
```
and
```yaml
rated-power-flux:
  description: Ratio between the Rated Power of the device and the average Power production of the Device
  type: object
  properties:
    value:
      type: number
      format: float
    Aggregation-Level:
      $ref: Enums.yaml#/Aggregation-Level

array-capt-eff:
  description: Array Capture efficiency, defined as the ratio between Annual Array Captured Energy and Rated Power of the Array
  type: object
  properties:
    value:
      type: number
      format: float
    Aggregation-Level:
      $ref: Enums.yaml#/Aggregation-Level

device-capt-eff:
  description: Device Capture efficiency, defined as the ratio between Annual Device Captured Energy and Rated Power of the Device
  type: object
  properties:
    value:
      type: number
      format: float
    Aggregation-Level:
      $ref: Enums.yaml#/Aggregation-Level

Efficiency:
  type: object
  properties:
    rated-power-flux:
      $ref: #/rated-power-flux
    array-capt-eff:
      $ref: #/array-capt-eff
    device-capt-eff:
      $ref: #/device-capt-eff
```
As it is possible to see, the first version allows to avoid some duplications and keep OpenAPI more synchronized and structured. At the same time, to achieve this it was forced to apply some workarounds: ["You will always use $ref as the only key in an object: any other keys you put there will be ignored by the validator."](https://json-schema.org/understanding-json-schema/structuring.html) Therefore, in the following version `description` attributes will be finally ignored:
```yaml
rated-power-flux:
  $ref: #/number-attribute
  description: Ratio between the Rated Power of the device and the average Power production of the Device

array-capt-eff:
  $ref: #/number-attribute
  description: Array Capture efficiency, defined as the ratio between Annual Array Captured Energy and Rated Power of the Array

device-capt-eff:
  $ref: #/array-attribute
  description: Device Capture efficiency, defined as the ratio between Annual Device Captured Energy and Rated Power of the Device
```
*OCC* advice will be to focus on defining a context free business entities; not minimizing number of lines in your *OpenAPI* file. So, the final version could look like the following -
```yaml
rated-power-flux:
  description: Ratio between the Rated Power of the device and the average Power production of the Device
  type: number
  format: float

array-capt-eff:
  description: Array Capture efficiency, defined as the ratio between Annual Array Captured Energy and Rated Power of the Array
  type: number
  format: float

device-capt-eff:
  description: Device Capture efficiency, defined as the ratio between Annual Device Captured Energy and Rated Power of the Device
  type: number
  format: float

Efficiency:
  type: object
  properties:
    rated-power-flux:
      type: object
      properties:
        value:
          $ref: #/rated-power-flux
        Aggregation-Level:
          $ref: Enums.yaml#/Aggregation-Level
    array-capt-eff:
      type: object
      properties:
        value:
          $ref: #/array-capt-eff
        Aggregation-Level:
          $ref: Enums.yaml#/Aggregation-Level
    device-capt-eff:
      type: object
      properties:
        value:
          $ref: #/device-capt-eff
        Aggregation-Level:
          $ref: Enums.yaml#/Aggregation-Level
```

## "Is it possible to use Pandas objects and how?"
Exposing *Pandas* objects in *OpenAPI* could have a reason if the following conditions are met:
1. Most of your *consumers* are familiar with *Pandas*
2. These *consumers* will install and use *Pandas* any way
   * *Pandas* is like an industrial standard (even *Digital Representation* could include it)

If, additionally, there are no foreseeable performance issues for transferring *Pandas* object (they are small) then
1. *OCC* would recommend keeping *Pandas* related response in a human readable format
1. *OCC* would prefer *JSON*, because while been readable it is easier to handle it programmatically
1. *Pandas* *JSON* representation is mapped in *OpenAPI* as a `string` data type

Let's consider the following example
```yaml
pandas-table:
  type: object
  properties:
    value:
      type: string
    Aggregation-Level:
      $ref: Enums.yaml#/Aggregation-Level

array_annual_gross_energy_pd:
  description: (Pandas table) the annual gross energy of the array during all the life. it isa pandas table with just one column, the index are the years. the index are the years (the same values are repeated); the only column is"Annual Energy" useful for interacting with the other methods
  $ref: #/pandas-table

Energy-Production:
  type: object
  properties:
    array_annual_gross_energy_pd:
      $ref: #/array_annual_gross_energy_pd
 ```
 in the sense of [another](# "How to add description on top of an existing definition?") discussion, it had better be represented in the following way
 ```yaml
array_annual_gross_energy_pd:
  description: (Pandas table) the annual gross energy of the array during all the life. it isa pandas table with just one column, the index are the years. the index are the years (the same values are repeated); the only column is"Annual Energy" useful for interacting with the other methods
  type: string

Energy-Production:
  type: object
  properties:
    array_annual_gross_energy_pd:
      type: object
      properties:
        value:
          $ref: #/array_annual_gross_energy_pd
        Aggregation-Level:
          $ref: Enums.yaml#/Aggregation-Level
```
