# Representation

A module can support creating _digital representation_ of all its data and restoring it back. Main module provides user with an API for downloading or uploading data for all the modules at once, see [dtop_main's documentation on this subject](https://opencascade.gitlab.io/dtocean/dtop_main/representation.html). For this to work, each module must support the same standardized API.


## Tutorial

This tutorial describes how to add representation support into your project.


#### Implement `GET /representation/{id}` and `POST /representation`

Main module's tests contain minimal working example of OpenAPI with representation support: [`openapi.yaml`](https://gitlab.com/opencascade/dtocean/dtop_main/blob/master/tests/service/representables/module1/openapi.yaml#L0). We recommend copying declarations of both `GET /representation/{id}` and `POST /representation` to the **paths** section of your module's OpenAPI. Also, don't forget to copy **representation** component into **components.schemas** section.

Most likely, you want your GET method to produce data in same format as your POST method accepts, that's why we recommend make GET and POST both reference the same schema component. In this tutorial, we will assume that they both reference **components.schemas.representation**, as in the example.

Modify the schema according to actual structure of data related to your module.


#### Implement `GET /representation/filters` _(optional)_

By default, when making `POST /representation` request to your module, main module sends all available data, leaving it up to you to decide which fields to use and which to ignore. This may lead to transfering too much data over network. If this becomes cricital, you can speed up the process by explicitly declaring list of fields that your module supports. If you do so, the main module will never send you fields outside that list.

The path schema that you need to implement for this is **/representation/filters**. You can copy it directly from the example [`openapi.yaml`](https://gitlab.com/opencascade/dtocean/dtop_main/blob/master/tests/service/representables/module1/openapi.yaml#L0) and insert without any modifications into **paths** section of your OpenAPI.

Then implement this route in your code. It has to return object with single field `paths` containing a list. Each item in the list should be a [JSON path](https://goessner.net/articles/JsonPath/) to a location in document that you module wants to be able to import. An implementation of `GET /representation/filters` can be found in [`app.py`](https://gitlab.com/opencascade/dtocean/dtop_main/blob/master/tests/service/representables/module1/app.py) of the minimal working example.)


#### Add fields to [`dr-schema.yaml`](../dr-schema.yaml) and [`dr-document.yaml`](../dr-document.yaml)

Besides providing OpenAPI schema for their own module, each author is expected to add their module's fields into two files:

- [`dr-schema.yaml`](../dr-schema.yaml) Schema containing fields from all schemas, combined.
- [`dr-document.yaml`](../dr-document.yaml) Document example containing fields from all document examples, combined.

These files will be used as references when performing some automated checks, see below.


## Merging files and analyzing conflicts

The repository contains a tool for merging and checking representation schemas from all `openapi.yaml` files. To run it, just type one command:

    make dr

Or, you can manually go through three individual commands instead:

    make dr-merge
    make dr-check-schema
    make dr-check-document


#### `make dr-merge`

This command tries to create a merged schema and a merged document, i.e. automatically generated analogs of [`dr-schema.yaml`](../dr-schema.yaml)/[`dr-document.yaml`](../dr-document.yaml). On successful generation, they will be saved locally as `dr-auto-schema.yaml`/`dr-auto-document.yaml`.

If one or two modules have declared the same data, it is considered a problem. In such case, the tool's output will contain messages like this:

    Conflict for 'app.data.version':
      module-1/0.0.1/openapi.yaml
      module-2/0.0.1/openapi.yaml

The message here says that two modules _module-1_ and _module-2_ contain path **data.common.app_version** in their declarations of **/representation/{id}**. This means that one of the two modules has to change its declaration to avoid the conflict. A branch cannot be merged into `master` until all conflicts are resolved.

If you believe that your module's OpenAPI is OK and other module's is not, create an issue or a merge request with explanation of the problem, assign it to other module's author and discuss it with them.

Note that the script checks not the entire schema's «tree», but only its «leaves». E.g., if two modules declare a _dictionary_ at `app.data`, it is not considered a conflict, and they both can use this common space for placeing their independent data. On the other hand, if they both declare a _string_ at `app.data.version`, it is a conflict, because the tool would be unable to merge two representation both containing such field.

After generating the files, the script will report fields in schema that do not match [`dr-schema.yaml`](../dr-schema.yaml). This means that if a field either is not declared there or is declared with another type, it will be considered a critical error. Similar check will be done for the document against [`dr-document.yaml`](../dr-document.yaml).


#### `make dr-check-schema`

This command reports fields that are declared in [`dr-schema.yaml`](../dr-schema.yaml) but are not present in automatically generated `dr-auto-schema.yaml`.

Such situation may happen, for example, if some fields were removed from module but left untouched in [`dr-schema.yaml`](../dr-schema.yaml). Or, if [`dr-schema.yaml`](../dr-schema.yaml) is being updated in advance, this may indicate that some of modules have not yet implemented what is expected from them. Anyway, this situation is not considered critical. In GitLab CI, the job `dr-check-schema` is marked as optional, and its failure is never considered a problem.


#### `make dr-check-document`

This command reports fields that are declared in [`dr-document.yaml`](../dr-document.yaml) but are not present in automatically generated `dr-auto-document.yaml`.

This works similar to `make dr-check-schema`, except operates not schemas, but document examples. It is also not considered critical. In GitLab CI, the job `dr-check-document` is marked as optional, and its failure is never considered a problem.


## Known limitations

The dr-merge tool in `api-cooperative` repository currently only recreates a basic structure of nested dictionaries from root down to each non-dictionary item, and then it just copies original item to the same place in resulting schema. It does not analyze any additional keywords in nested dictionaries, such as [`required`](https://json-schema.org/understanding-json-schema/reference/object.html#required-properties), [`minProperties`](https://json-schema.org/understanding-json-schema/reference/object.html#size), and so on.

If you need to use such keywords to express your OpenAPI, feel free to create an issue in this repository. In the issue, please explain which keyword you want to be able to use and how do you think it should affect the dr-merge tool behaviour. Without proper discussion on supporting each keyword, we cannot guarantee that it won't produce either false positives or false negatives when analyzing such schemas.


## FAQ

#### CI job `dr-merge` fails. Does it mean that my OpenAPI is incorrect?

Not neccessarily. It just means that some other module contains same fields as yours. It can be as simple as a copy-paste mistake, or you could accidentally come up with the same name for your independent pieces of data. Create an issue or a merge request and discuss the conflict with your colleague working on other module. As soon as you come up with a conflict-free solution and commit your modifications, CI job should run without errors.


#### Does `make dr-merge` actually runs my code?

No. The dr-merge tool only works with schemas to find overlaps between them. To make sure that schemas properly desribe code, modules should use other tools, such as [Dredd](https://dredd.org/). Dredd will perform queries to all modules declared in your OpenAPI (including **/representation**, once you've added it there) and report if any of them don't seem to have an implementation or return wrong data type.


#### Why do I get 'Error opening file' messages when running `make dr-merge`?

The dr-merge tool implementation reads source `openapi.yaml` files but sometimes thay may contain references to bundled `openapi.yml` files (note the different filename extension). When those files are not generated, it may cause errors on some modules. This should not normally happen because command `make dr-merge` automatically creates missing YML files before running, but if you just modified some module's `openapi.yaml`, you may encounter the error. We recommend running `make reset` and then trying again.

(Actually, this is not a merge-specific problem: you can sometimes get the same message running `make bundle` after modifying some files.)
