.PHONY: $(wildcard x-*)

#MAKEFLAGS += --no-builtin-rules
SHELL = /bin/bash

YAML = $(wildcard dtop-*/*/openapi.yaml)
YML  = $(YAML:.yaml=.yml)
JSON = $(YAML:.yaml=.json)
HTML = $(shell echo $(YAML) | sed 's:\(\S\+\)/[0-9.]\+/openapi.yaml:public/\1.html:g')

NODE_OPTIONS:=--max-old-space-size=3072

all:
	docker-compose build test
	docker-compose run --rm test make x-all

x-all: x-reset x-test x-lint x-bundle x-pages x-test-code x-dr-merge


#---------------------------------------------------------------------------------------------------
# Aliases for those who are used to older versions of this file

pre: lint
xenv-local: x-environment-prepare
xtest: test
xdev: dev
xenv: rebuild


#---------------------------------------------------------------------------------------------------
# Clean generated files

reset:
	docker-compose run --rm test make x-reset

x-reset:
	rm -rf $(JSON) $(YML) public dr-auto-schema.yaml dr-auto-document.yaml .coverage


#---------------------------------------------------------------------------------------------------
# Rebuild Docker image

rebuild:
	docker-compose build --no-cache test


#---------------------------------------------------------------------------------------------------
# Prepare environment

x-environment-prepare:
	npm install -g $(shell cat node-packages.txt)

environment-update:
	docker-compose build environment-update
	docker-compose run --rm environment-update

x-environment-update:
	make x-all
	conda env export --no-builds --name api-cooperative > environment.yml


#---------------------------------------------------------------------------------------------------
# Launch console

dev:
	docker-compose run --rm test /bin/bash


#---------------------------------------------------------------------------------------------------
# Run pre-commit hooks

lint:
	docker-compose run --rm test make x-lint

x-lint:
	pre-commit run --all-files
	@echo "---------------------------------------- OK ----------------------------------------"


#---------------------------------------------------------------------------------------------------
# Convert YAMLs to bundled YMLs

bundle:
	docker-compose run --rm test make x-bundle

x-bundle: x-pre-order $(YML)
	@echo "---------------------------------------- OK ----------------------------------------"

%.yml: %.yaml
	swagger-cli bundle --dereference -o $@ -t yaml $<


#---------------------------------------------------------------------------------------------------
# Convert YAMLs to JSONs

test:
	docker-compose run --rm test make x-test

x-test: x-pre-order $(JSON)
	./check_references.py
	@echo "---------------------------------------- OK ----------------------------------------"

x-pre-order: \
	dtop-sc/0.0.1/openapi.yml \
	dtop-mc/0.0.1/openapi.yml \
	dtop-ec/0.0.1/openapi.yml \
	dtop-et/0.0.1/openapi.yml \
	dtop-ed/0.0.1/openapi.yml \
	dtop-sk/0.0.1/openapi.yml \
	dtop-lmo/0.0.1/openapi.yml \
	dtop-spey/0.0.1/openapi.yml \
	dtop-rams/0.0.1/openapi.yml \
	dtop-slc/0.0.1/openapi.yml \
	dtop-esa/0.0.1/openapi.yml \
	dtop-structinn/0.0.1/openapi.yml \
	dtop-sg/0.0.1/openapi.yml \
	dtop-catalog/0.0.1/openapi.yml \
	dtop-mm/0.0.1/openapi.yml

pre-order:
	docker-compose run --rm test make x-pre-order

%.json: %.yaml
	openapi-spec-validator $<
	swagger-cli validate $<
	swagger-cli bundle --dereference -o $@ -t json $<
#	NODE_OPTIONS=${NODE_OPTIONS} speccy lint $@
	swagger-cli validate $@
	openapi-spec-validator $@
	node --unhandled-rejections=strict `which openapi-examples-validator` -- $@

#---------------------------------------------------------------------------------------------------
# Check examples against corresponding schemas

%.examples-check: %.json
	node --unhandled-rejections=strict `which openapi-examples-validator` -- $<

%.examples: pre-order
	docker-compose run --rm test make ${@}-check

x-examples: \
	dtop-sc/0.0.1/openapi.examples-check \
	dtop-ec/0.0.1/openapi.examples-check \
	dtop-et/0.0.1/openapi.examples-check \
	dtop-ed/0.0.1/openapi.examples-check \
	dtop-lmo/0.0.1/openapi.examples-check \
	dtop-rams/0.0.1/openapi.examples-check \
	dtop-slc/0.0.1/openapi.examples-check \
	dtop-structinn/0.0.1/openapi.examples-check \
	dtop-sg/0.0.1/openapi.examples-check

examples:
	docker-compose run --rm test make x-${@}

#---------------------------------------------------------------------------------------------------
# Check representation schemas

dr:
	docker-compose run --rm test make x-dr
dr-merge:
	docker-compose run --rm test make x-dr-merge
dr-check:
	docker-compose run --rm test make x-dr-check
dr-check-schema:
	docker-compose run --rm test make x-dr-check-schema
dr-check-document:
	docker-compose run --rm test make x-dr-check-document

x-dr: x-dr-merge x-dr-check

x-dr-merge: x-pre-order $(YML)
	./dr.py merge --output-schema dr-auto-schema.yaml --output-document dr-auto-document.yaml

x-dr-check: x-dr-check-schema x-dr-check-document

x-dr-check-schema:
	./dr.py check-schema dr-auto-schema.yaml

x-dr-check-document:
	./dr.py check-document dr-auto-document.yaml


#---------------------------------------------------------------------------------------------------
# Run Python tests

test-code:
	docker-compose run --rm test make x-test-code

x-test-code:
	python -m pytest tests -vv --cov=. --cov-fail-under=100


#---------------------------------------------------------------------------------------------------
# Generate HTML documentation

pages:
	docker-compose run --rm test make x-pages

x-pages: x-pages-index x-pre-order $(HTML)

x-pages-index:
	mkdir -p public
	echo $(HTML) | sed 's:\s*public/\(\S\+\)\.html:<a href="\1.html">\1</a><br/>\n:g' > public/index.html

public/%.html: %/*/openapi.yml
	redoc-cli bundle --output $@ $<
