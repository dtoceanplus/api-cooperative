#!/usr/bin/env python

import re
import sys
from collections import defaultdict
from contextlib import suppress
from pathlib import Path
from typing import Dict, Iterable, List

import yaml


def main(root: Path = Path(__file__).parent.absolute()) -> int:
    """
    Check that if a module references another module, it uses only its GET requests.
    A module is any directory that is located exactly two directories deep into `root`.
    Any files inside a module are considered local and can be referenced without limitations.
    """
    filenames: List[Path] = []
    for filename in sys.argv[1:] or list(root.glob('dtop-*')) + [root/'common']:
        filename = Path(filename).absolute()
        filenames += filename.rglob('*.yaml') if filename.is_dir() else [filename]
    filenames.sort()

    bad_refs: Dict[str, List[str]] = defaultdict(list)
    for filename in filenames:
        filename = Path(filename).absolute()
        current_dir = filename.parent
        module_root = root.joinpath(*filename.relative_to(root).parts[:1])

        print(f'Checking references in {filename.relative_to(root)}...', file=sys.stderr)

        with open(filename) as file:
            schema = yaml.load(file, Loader=yaml.SafeLoader)
            for ref in find_references(schema):
                path_and_pointer = ref.split('#', maxsplit=1)
                path = (current_dir / path_and_pointer[0]).resolve()
                pointer = path_and_pointer[1] if len(path_and_pointer) > 1 else ''

                allowed = False

                # In current module's directory, one can use any components
                with suppress(ValueError):
                    path.relative_to(module_root)
                    allowed = True

                # In 'common' directory, one can use any components
                with suppress(ValueError):
                    path.relative_to(root / 'common')
                    allowed = True

                # In other cases, only content from GET requests can be used
                allowed = allowed or re.fullmatch(
                    r'/paths/.+/get/responses/200/content/application~1json/schema', pointer)

                if not allowed:
                    bad_refs[filename.relative_to(root)].append(ref)

    if bad_refs:
        print('\nFollowing references are not allowed between modules:', file=sys.stderr)
        for filename, bad_refs_in_file in sorted(bad_refs.items()):
            print(f'  in {filename}:', file=sys.stderr)
            for ref in bad_refs_in_file:
                print(f'    {ref}', file=sys.stderr)
        print(file=sys.stderr)
        print('Please replace them with references to other GET responses.', file=sys.stderr)
        print(file=sys.stderr)
        return 1

    return 0


def find_references(data) -> Iterable[str]:
    if isinstance(data, dict):
        try:
            yield data['$ref']
        except KeyError:
            for value in data.values():
                yield from find_references(value)
    elif isinstance(data, list):
        for value in data:
            yield from find_references(value)


if __name__ == '__main__':  # pragma: no cover
    exit(main() or 0)
