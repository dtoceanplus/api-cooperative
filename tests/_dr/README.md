Tests for Digital Representation
================================

This directory contains test cases for [`test_dr.py`](../test_dr.py). Each subdirectory is a separate test case. Number of files in a sundirectory can vary depending on how far the test is expected to go before failing. Below is the scenario for a test case that is expected to be entirely successful.

0. All files `?.yaml` (with one-symbol names) are the files that are going to be merged using using [`dr.py merge`](../../dr.py). As usually, it can involve comparing results of merge with `dr-schema.yaml` and `dr-document.yaml`.

0. Results of merge are compared with `expected-schema.yaml` and `expected-document.yaml`. If such files are not presented in a given testcase, it is assumed that the files are expected to be empty.

0. All output in `stderr` up to this point is then compared to `expected-stderr-1.txt`. Also, the final line of the file must contain expected exit code for the merge command.

0. If all went successfully, the test calls [`dr.py check-schema`](../../dr.py) to compare resulting schema (essentially the `expected-schema.yaml`) with `dr-schema.yaml` provided by the test. After this step, all new output in `stderr` is compared to `expected-stderr-2.txt`. Also, the final line of the file must contain expected exit code for the check-schema command.


To run these tests, use this command:

    python -m pytest -v tests/test_dr.py

To run a specific test case, use pytest's parametrized run syntax:

    python -m pytest -v tests/test_dr.py -k test_dr[array_with_compatible_ids]
    python -m pytest -v tests/test_dr.py -k test_dr[array_with_incompatible_ids]
    ...
