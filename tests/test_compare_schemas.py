from typing import Dict, Iterable

import pytest

from dr import compare_schemas, SchemaComparison


@pytest.mark.parametrize(('schema1', 'schema2',
                          'direct_comparison_result', 'inverted_comparison_result'), (
    pytest.param(
        {},
        {},
        SchemaComparison(), SchemaComparison(),
        id='identical_empty_objects_1'
    ),
    pytest.param(
        {'type': 'object', 'properties': {}},
        {},
        SchemaComparison(), SchemaComparison(),
        id='identical_empty_objects_2'
    ),
    pytest.param(
        {'properties': {'properties': {'properties': {}}}},
        {'properties': {'properties': {'properties': {}}}},
        SchemaComparison(), SchemaComparison(),
        id='identical_empty_objects_inside_objects'
    ),
    pytest.param(
        {'properties': {'one': {'type': 'integer'}}},
        {'properties': {'one': {'type': 'integer'}, 'two': {'type': 'integer'}}},
        SchemaComparison(), SchemaComparison(extra_paths={'two'}),
        id='missing_property_on_1st_level'
    ),
    pytest.param(
        {'properties': {'one': {'type': 'integer'}}},
        {'properties': {'one': {'type': 'float'}}},
        SchemaComparison(wrong_types={'one'}), SchemaComparison(wrong_types={'one'}),
        id='integer_and_float'
    ),
    pytest.param(
        {'properties': {'one': {'type': 'integer'}}},
        {'properties': {'one': {'type': 'object'}}},
        SchemaComparison(wrong_types={'one'}), SchemaComparison(wrong_types={'one'}),
        id='integer_and_object'
    ),
))
def test_compare_schemas(schema1: Dict, schema2: Dict,
                         direct_comparison_result: SchemaComparison,
                         inverted_comparison_result: SchemaComparison):
    assert compare_schemas(schema1, schema2) == direct_comparison_result
    assert compare_schemas(schema2, schema1) == inverted_comparison_result
