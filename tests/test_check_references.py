import sys
from io import StringIO
from pathlib import Path

import pytest

from check_references import main


@pytest.mark.parametrize('directory', (pytest.param(x, id=x.name)
                                       for x in sorted((Path(__file__).parent / '_check_references').iterdir())
                                       if x.is_dir() and x.name[0].isalpha()))
def test_check_references(directory: Path):
    # Find all YAML with one character in the name
    old_argv = sys.argv
    sys.argv = ['test', str(directory)]
    sys.stderr = fake_stderr = StringIO()
    try:
        exit_code = main(directory)
    finally:
        sys.argv = old_argv
        sys.stdout = sys.__stdout__
        sys.stderr = sys.__stderr__

    # Check log in stderr and exit code
    with (directory / 'stderr.txt').open() as file:
        expected_stderr, expected_exit_code = file.read().strip().rsplit('\n', 1)
        assert fake_stderr.getvalue().strip() == expected_stderr
        assert exit_code == int(expected_exit_code)
