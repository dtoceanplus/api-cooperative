import os
import sys
from contextlib import suppress
from glob import glob
from io import StringIO
from os.path import exists
from pathlib import Path
from tempfile import TemporaryDirectory
from typing import Tuple

import pytest

from dr import main, run_merge


@pytest.mark.parametrize('directory', (pytest.param(x, id=x.name)
                                       for x in sorted((Path(__file__).parent / '_dr').iterdir())
                                       if x.is_dir() and x.name[0].isalpha()))
def test_dr(directory):
    old_cwd = os.getcwd()
    os.chdir(directory)
    try:
        with TemporaryDirectory() as tmpdir:
            # Step one: merge
            output_schema = None
            output_document = None
            exit_code, stderr = _run_dr('merge',
                                        *sorted(glob('?.yaml')),
                                        '--output-schema', f'{tmpdir}/dr-auto-schema.yaml',
                                        '--output-document', f'{tmpdir}/dr-auto-document.yaml')
            with open(f'{tmpdir}/dr-auto-schema.yaml') as file:
                output_schema = file.read()
            with open(f'{tmpdir}/dr-auto-document.yaml') as file:
                output_document = file.read()

            # Check unified schema
            expected_schema = ''
            with suppress(FileNotFoundError):
                with open('expected-schema.yaml') as file:
                    expected_schema = file.read()
            assert output_schema == expected_schema

            # Check unified document
            expected_document = ''
            with suppress(FileNotFoundError):
                with open('expected-document.yaml') as file:
                    expected_document = file.read()
            assert output_document == expected_document

            # Check stderr and exit code
            with open('expected-stderr-1.txt') as file:
                expected_stderr, expected_exit_code = file.read().rstrip().rsplit('\n', 1)
                assert stderr == expected_stderr
                assert exit_code == int(expected_exit_code)

            # Step two: check schema
            if exists('dr-schema.yaml'):
                exit_code, stderr = _run_dr('check-schema', f'{tmpdir}/dr-auto-schema.yaml')
                try:
                    with open('expected-stderr-2.txt') as file:
                        expected_stderr, expected_exit_code = file.read().rstrip().rsplit('\n', 1)
                except FileNotFoundError:
                    expected_stderr, expected_exit_code = '', 0
                assert stderr == expected_stderr
                assert exit_code == int(expected_exit_code)

    finally:
        os.chdir(old_cwd)


def _run_dr(*args: str) -> Tuple[int, str]:
    old_argv = sys.argv
    sys.argv = ['dr', *args]
    sys.stderr = StringIO()

    try:
        main()
    except SystemExit as exc:
        return exc.code, sys.stderr.getvalue().strip()
    else:
        return 0, sys.stderr.getvalue().strip()
    finally:
        sys.argv = old_argv
        sys.stderr = sys.__stderr__
