import pytest

from check_references import find_references


@pytest.mark.parametrize(('data', 'expected'), (
    pytest.param(
        {},
        (),
        id='empty_dict'),
    pytest.param(
        [],
        (),
        id='empty_list'),
    pytest.param(
        {
            'x1': {
                'y1': {
                    'z1': {'$ref': 'reference from z1'},
                    'z2': {'$ref': 'reference from z3'},
                },
                'y2': [
                    {'$ref': 'reference from y2 - 0'},
                    {'$ref': 'reference from y2 - 1'},
                    {'$ref': 'reference from y2 - 2'},
                ]
            },
            'x2': {'$ref': 'reference from x2'},
        },
        ('reference from z1',
         'reference from z3',
         'reference from y2 - 0',
         'reference from y2 - 1',
         'reference from y2 - 2',
         'reference from x2'),
        id='some_references'),
))
def test_find_references(data, expected):
    assert tuple(find_references(data)) == expected
