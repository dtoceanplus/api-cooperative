import os
import sys
from pathlib import Path
from unittest.mock import Mock, call, patch

from dr import find_files, main


def test_find_files():
    old_cwd = os.getcwd()
    os.chdir(Path(__file__).parent / '_find_files')
    try:
        assert tuple(find_files(None)) == (
            Path('module1/openapi.yaml'),
            Path('module2/openapi.yaml'),
            Path('module2/parts/openapi.yaml'))
    finally:
        os.chdir(old_cwd)
