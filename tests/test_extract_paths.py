import pytest

from dr import ArrayDataItem, DataItem, ExclusiveDataItem, PrimitiveDataItem, extract_paths


@pytest.mark.parametrize(('source', 'paths'), (
    pytest.param(
        {},
        (),
        id='empty_data'),

    pytest.param(
        {'type': 'integer'},
        (),
        id='scalar_root'),

    pytest.param(
        {'properties': {
            'x1': {'type': 'number'},
            'x2': {'type': 'string'},
            'x3': {'type': 'boolean'},
        }},

        (PrimitiveDataItem('x1', {'type': 'number'}),
         PrimitiveDataItem('x2', {'type': 'string'}),
         PrimitiveDataItem('x3', {'type': 'boolean'})),

        id='only_level_0'),

    pytest.param(
        {'properties': {
            'x': {'properties': {
                'y1': {'type': 'number'},
                'y2': {'type': 'string'},
                'y3': {'type': 'boolean'},
            }},
        }},

        (PrimitiveDataItem('x.y1', {'type': 'number'}),
         PrimitiveDataItem('x.y2', {'type': 'string'}),
         PrimitiveDataItem('x.y3', {'type': 'boolean'})),

        id='only_level_1'),

    pytest.param(
        {'properties': {
            'x': {'properties': {
                'y': {'properties': {
                    'z1': {'type': 'number'},
                    'z2': {'type': 'string'},
                    'z3': {'type': 'boolean'},
                }},
            }},
        }},

        (PrimitiveDataItem('x.y.z1', {'type': 'number'}),
         PrimitiveDataItem('x.y.z2', {'type': 'string'}),
         PrimitiveDataItem('x.y.z3', {'type': 'boolean'})),

        id='only_level_3'),

    pytest.param(
        {'properties': {
            'x1': {'properties': {
                'y1': {'properties': {
                    'z1': {'type': 'number'},
                    'z2': {'type': 'string'},
                }},
                'y2': {'type': 'boolean'},
            }},
            'x2': {'type': 'integer'},
        }},

        (PrimitiveDataItem('x1.y1.z1', {'type': 'number'}),
         PrimitiveDataItem('x1.y1.z2', {'type': 'string'}),
         PrimitiveDataItem('x1.y2', {'type': 'boolean'}),
         PrimitiveDataItem('x2', {'type': 'integer'})),

        id='multiple_levels'),

    pytest.param(
        {'properties': {
            'x1': {'properties': {}},
            'x2': {},
        }},

        (PrimitiveDataItem('x1', {'properties': {}}),
         PrimitiveDataItem('x2', {})),

        id='no_properties'),

    pytest.param(
        {'properties': {
            'x': {'type': 'array',
                  'items': {'type': 'string'}},
        }},

        (ArrayDataItem('x'),
         PrimitiveDataItem('x[*]', {'type': 'string'})),

        id='array_of_scalars'),

    pytest.param(
        {'properties': {
            'x': {'type': 'array',
                  'items': {'properties': {'y': {'type': 'string'}}}},
        }},

        (ArrayDataItem(    'x'),
         PrimitiveDataItem('x[*].y', {'type': 'string'})),

        id='array_of_objects'),

    pytest.param(
        {'properties': {
            'x': {'properties': {
                'one': {'properties': {'y': {'type': 'string'}}},
                'two': {'properties': {'y': {'type': 'string'}}},
                'three': {'properties': {'y': {'type': 'string'}}},
                'connection': {'properties': {'y': {'type': 'string'}}},
                'properties': {'properties': {'y': {'type': 'string'}}},
                'location': {'properties': {'y': {'type': 'string'}}},
                'hierarchical': {'properties': {'y': {'type': 'string'}}},
            }}
        }},

        (PrimitiveDataItem('x.one.y', {'type': 'string'}),
         PrimitiveDataItem('x.two.y', {'type': 'string'}),
         PrimitiveDataItem('x.three.y', {'type': 'string'}),
         ExclusiveDataItem('x.connection.y', {'type': 'string'}),
         ExclusiveDataItem('x.properties.y', {'type': 'string'}),
         ExclusiveDataItem('x.location.y', {'type': 'string'}),
         ExclusiveDataItem('x.hierarchical.y', {'type': 'string'})),

        id='children_of_special_names'),
))
def test_extract_paths(source, paths):
    unsupported = set()
    assert tuple(extract_paths(source, unsupported)) == paths
    assert len(unsupported) == 0
