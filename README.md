# Cooperative API design
Demonstrates how to perform cooperative API design

## Rational
The main idea behind this cooperative API design is to put all the DTOceanPlus OpenAPIs together to enable sharing of overlapping business domain logic via referencing and sharing common entity definitions. As result the following benefits could be achieved:
1. Business domain logic will be located at the most appropriate places
1. Digital representation will take a shape and emerge in a formal description

## Examples
Let's imagine that we have the following OpenAPIs repository structure:
```
+ dtop-provider/1.0
|   + openapi.yaml
+ dtop-consumer/1.0
|   + openapi.yaml
+ common
    + schemas.yaml
```
### Referencing to a *provider* definition from a *consumer*
To use a data structure defined in another module, use the `$ref` tag. Note that here in `api-cooperative` we allow only references to other modules' GET responses. Any schema component outside a GET request is considered private and for in-module use only.

When referencing a GET request, you have to replace `/` to `~1` in the path. For example, `/some/path` becomes `~1some~1path`.

```yaml
components:
  schemas:
    Id:
      $ref: "../../1.0/dtop-provider/openapi.yaml#/paths/~1some~1path/get/responses/200/content/application~1json/schema"
```
### Sharing a *common* business entity definition
Components that are placed in files in `common` directory can be referenced from any module.

```yaml
components:
  schemas:
    Entity:
      $ref: "../../common/schemas.yaml#/Entity"
```

To get OpenAPI referencing techniques in more details look [here](https://swagger.io/docs/specification/using-ref/)

### Tips and tricks
* **Important** that the primary goal is to come up and use the same business domain representation (digital representation).
   * Generalization of other OpenAPI features, like *"responses"*, has much less importance.
* Since each module has its unique responsibilities, *"paths"* section is expected to be unique for each module. There are no obvious reasons to generalize it; might be only to keep the same style and naming convention.
* To keep *"paths"* section stable, actual business logic *sharing* had better happen at OpenAPI *"components"* section. For example
```yaml
paths:
  /entities:
    post:
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Entity'
components:
  schemas:
    Entity:
      $ref: "../../dtop-provider/1.0/openapi.yaml#/components/Entity"
```

* If your module *provider* counterpart is *"not ready"* yet, try to come up with your own understanding/description of the business entities your are consuming from him. Then communicate your proposal back to the the *provider*.
* Guide your module API design by the *Robustness Principle*: ["*your API must be conservative in what it provides and flexible in what it consumes".*](https://martinfowler.com/articles/consumerDrivenContracts.html)

## Installation

1. Clone the code via `git clone --recurse-submodules <repo url>` command
1. Install [docker-compose](https://docs.docker.com/compose/install/)
1. Install [pre-commit](https://pre-commit.com/)
1. Install [make](https://anaconda.org/anaconda/make)

Sometimes, when [Dockerfile](./Dockerfile) is modified (usually could happen after rebasing to the `master` via "`git rebase origin/master`") a weird behaviour could be experienced. This happens because corresponding `docker-compose` images become deprecated. To overcome this issue you should run `make rebuild` to rebuild the image.

## Validation

- `make test` (or `docker-compose run --rm xtest`)
- `make lint` (or `pre-commit run --all-files`)

## Documentation

You can view API documentation on GitLab Pages: https://opencascade.gitlab.io/dtocean/sandbox/api-cooperative

Alternatively, you can generate HTML documentation locally:

- `make pages`

### Note for Windows machines
Docker was not originaly built for Windows environment and, for this reason, you may experience some bugs while trying to run the `docker-compose` tests.\
If you try to run validation and you get something like this:
```text
make: *** No rule to make target 'test'.  Stop.
```
or
```text
./check_references.py
/usr/bin/env: ‘python\r’: No such file or directory
make: *** [Makefile:99: x-test] Error 127
```
please follow the next steps:
* Turn off your firewall;
* Right click on **Docker** (it is probably hidden in your taskbar - bottom right corner) and click settings;
* Go to **Shared Drives** (in the most recent version of Docker Desktop [2.2.0.4], the new location is **Resources > File sharing**), untick the **C** checkbox, click **Apply**, tick it again and click **Apply** again;
* Assuming that you already clonned the repository to your local machine, go to **api-cooperative** folder;
* Run `git config core.autocrlf false`
* Run `git rm --cached -r . `
* Run `git reset --hard` (Attention: this will destroy all your uncommited changes.)
* Run validation.

## Running stub servers
A stub version of a server can be launched using its API definition. This may be useful if you are devloping a module that uses another module's API and want to see how the module is probably going to respond to a certain request even if that other module hadn't yet actually implemented this particular functionality.

1. Install [connexion](https://connexion.readthedocs.io/)
2. Regenerate bundled versions of API definitions with `make bundle`
3. Run a server using this command (replace the path with the path to yml file of the module you want):
```bash
connexion run --mock=all dtop-ec/0.0.1/openapi.yml
```

This will launch a Flask-based server that will try to mock as many endpoints as possible using the example values from API definition.

## Running stub servers
A stub version of a server can be launched using its API definition. This may be useful if you are devloping a module that uses another module's API and want to see how the module is probably going to respond to a certain request even if that other module hadn't yet actually implemented this particular functionality.

1. Install [connexion](https://connexion.readthedocs.io/)
2. Regenerate bundled versions of API definitions with `make bundle`
3. Run a server using this command (replace the path with the path to yml file of the module you want):
```bash
connexion run --mock=all dtop-ec/0.0.1/openapi.yml
```

This will launch a Flask-based server that will try to mock as many endpoints as possible using the example values from API definition.

## Suggested workflow
1. Fork this repository
1. *Publish* or advance your module Open API definition
   * *Publishing* happens via putting your current OpenAPI definition into corresponding `dtop-` + API `version` prefixed folder. For example `dtop-provider/1.0`.
   * Make sure that business entities definitions your module is consuming are taken from a *right place*.
   * Directly refer to a *provider* module definition when you consuming *his* entity
   * To avoid possible cyclic dependencies it is suggested to agree between entity *provider(s)* and *consumer(s)* to use `common` library for the entity definition
   * If your module was published correctly you should see that it is mentioning somehow into the test procedure output.
1. Test that your modifications are compatible with other module definitions.
   * Test procedure is automated and performs the following checks
     * Perform all possible static checks.
     * Make sure that an independent OpenAPI bundle could be finally generated.
     * This package comes with GitLab native **C**ontinues **I**ntegration support. So, before suggesting the resulting merge request, make sure your commit pass GitLab CI
1. Propose merge request
   * Explicitly `assign` it to @dtoceanplus.integration
   * Start you request `Title` with `WIP:`
   * Invite all *interested* partners into this merge request
     * Mention corresponding `@nickname` into the request `Description` body
   * Review and iterate on this merge request among all the invited partners
     * Share what you have learned via extending `README.md`
   * Once everybody agree on the merge request content indicate it via removing `WIP:` status
   * @dtoceanplus.integration will squash all the merge request commits and, finally, merge it into the common `master`
2. Use generated OpenAPI JSON bundle for implementing your module service.

## More detailed instructions
### [GitLab specific workflow description](./docs/detailed-instructions.md) (@vincenzo.nava)
### [Simplicity or Stability](https://gitlab.com/wave-energy-scotland/dtoceanplus/dtop-stagegate/wikis/Simplicity-or-Stability) trade offs to consider (@benhudson31)
### [Robustness Principle Practical Example](https://gitlab.com/wave-energy-scotland/dtoceanplus/dtop-stagegate/wikis/Robustness-Principle-Practical-Example) (@benhudson31)
### [Cook book and OpenAPI best practices](./docs/openapi-best-practices.md)
### [Avoid versioning](https://opensource.zalando.com/restful-api-guidelines/#113)

> When changing your RESTful APIs, do so in a compatible way and avoid generating additional API versions. Multiple versions can significantly complicate understanding, testing, maintaining, evolving, operating and releasing our systems.
>
> If changing an API can’t be done in a compatible way, then proceed in one of these three ways:
>
> * create a new resource (variant) in addition to the old resource variant
>
> * create a new service endpoint — i.e. a new application with a new API (with a new domain name)
>
> * create a new API version supported in parallel with the old API by the same microservice
>
> As we discourage versioning by all means because of the manifold disadvantages, we strongly recommend to only use the first two approaches.
